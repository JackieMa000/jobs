import difflib
from typing import List
from uuid import UUID

from api.base import BaseResource

EXTERNAL_DATA_HOST = 'http://api.dataatwork.org'

JOB_MAP_INTERNAL_TO_EXTERNAL = {
    'title_id': 'uuid', 'normalized_title': 'normalized_job_title'}


class Job(BaseResource):
    api_path = f'v1/jobs/autocomplete'

    def get(self, name: str) -> dict:
        self.name = name
        result = {}
        rsp = super().get(name)
        if rsp['code'] == 200:
            result = self.get_closest_match(name, rsp['data'])
        return result

    def get_url(self, *args, **kwargs) -> str:
        return f'{EXTERNAL_DATA_HOST}/{self.api_path}?begins_with={self.name}'

    @staticmethod
    def get_closest_match(name: str, jobs: List[dict]) -> dict:
        words = [item[JOB_MAP_INTERNAL_TO_EXTERNAL.get('normalized_title')] for item in jobs]
        # ToDo: Can come up with my own algorithm instead of using the 3rd-party liabrary.
        matched_list = difflib.get_close_matches(name.lower(), words, n=1)
        if not matched_list:
            return jobs[0]
        return jobs[words.index(matched_list[0])]


class Skills(BaseResource):
    def get(self, job_id: UUID) -> List[dict]:
        self.job_id = job_id
        result = []
        rsp = super().get(job_id)
        if rsp['code'] == 200:
            result = rsp['data']['skills']
        return result

    def get_url(self, *args, **kwargs) -> str:
        return f'{EXTERNAL_DATA_HOST}/{self.api_path}/{self.job_id}/related_skills'


class DataAPI():
    def __init__(self):
        self.job = Job()
        self.skills = Skills()
