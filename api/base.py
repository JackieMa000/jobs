from abc import ABCMeta, abstractmethod

import requests


class APIRequest():
    code = ''
    data = ''
    message = ''

    def request(self, method: str, url: str) -> dict:
        try:
            rsp = getattr(requests, method)(url)
        except requests.exceptions.Timeout:
            self.code = 100
            self.message = 'Connect timeout'
        except requests.exceptions.TooManyRedirects:
            self.code = 101
            self.message = 'Error connect to url'
        except requests.exceptions.RequestException as e:
            self.code = 101
            self.message = 'Error connect to url'
        else:
            if rsp.status_code == 400:
                self.code = 400
                self.message = 'Bad request'
            elif rsp.status_code == 401:
                self.code = 401
                self.message = 'Unauthorized request'
            elif rsp.status_code == 403:
                self.code = 403
                self.message = 'Forbidden Action'
            elif rsp.status_code == 404:
                self.code = 404
                self.message = 'Unknow action'
            elif rsp.status_code == 200:
                self.code = 200
                self.message = 'Success'
                self.data = rsp.json()
            else:
                pass

        return {
            'code': self.code,
            'data': self.data,
            'message': self.message
        }


class BaseResource(APIRequest, metaclass=ABCMeta):
    api_path = f'v1/jobs/'

    @abstractmethod
    def get_url(self, *args, **kwargs):
        pass

    def get(self, *args, **kwargs):
        return self.request('get', self.get_url(*args, **kwargs))

    def post(self, *args, **kwargs):
        return self.request('post', self.get_url(*args, **kwargs))