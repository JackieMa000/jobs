from typing import List
from uuid import UUID

from rest_framework import status
from rest_framework.response import Response

from api.models import Skill, Job
from api.providers import DataAPI, JOB_MAP_INTERNAL_TO_EXTERNAL

api = DataAPI()


def get_top_skills(title_id: UUID) -> List[Skill]:
    return Skill.objects.filter(job__title_id=title_id)[:10]


def get_job_by_id(title_id: UUID) -> Job:
    return Job.objects.get(title_id=title_id)


def get_job_list() -> List[Job]:
    return Job.objects.all()


def get_score_by_importance_and_level(importance: float, level: float) -> float:
    """
    Use proportional scoring as one technique to measure the score.
    For more info: http://mathforum.org/library/drmath/view/72033.html
    """
    return round((importance / 5 + level / 10) * 100, 2)


def get_skills_data_for_job(job_id: UUID) -> List[dict]:
    skills = api.skills.get(job_id)
    for i in range(len(skills)):
        skills[i]['score'] = get_score_by_importance_and_level(float(skills[i]['importance']),
                                                               float(skills[i]['level']))
    return skills


def generate_job_data(job_name: str, location: str) -> dict:
    job_result = api.job.get(job_name)
    title_id = job_result[JOB_MAP_INTERNAL_TO_EXTERNAL.get('title_id')]
    normalized_title = job_result[JOB_MAP_INTERNAL_TO_EXTERNAL.get('normalized_title')]
    skills_result = get_skills_data_for_job(title_id)
    if not job_result or not skills_result:
        return {}

    return {'title_id': title_id,
            'title': job_name,
            'normalized_title': normalized_title,
            'location': location,
            'skills': skills_result}


def verify_request_input(*params):
    def onDecorator(func):
        def onCall(instance, request, *args, **kwargs):
            if request.method == 'POST':
                for p in params:
                    if p not in request.data.keys():
                        return Response(f'Parameter {p} is missing',
                                        status=status.HTTP_400_BAD_REQUEST)
            return func(instance, request, *args, **kwargs)

        return onCall

    return onDecorator
