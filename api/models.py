from django.db import models


class Job(models.Model):
    """This class represents the job model."""
    title_id = models.CharField(max_length=150, unique=True, default='')
    title = models.CharField(max_length=150, default='')
    location = models.TextField()
    normalized_title = models.CharField(max_length=150, default='')
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        """Return a human readable representation of the model instance."""
        return "{} in {}".format(self.title, self.location)


class Skill(models.Model):
    skill_uuid = models.CharField(max_length=150, default="")
    skill_name = models.CharField(max_length=150, default="")
    skill_type = models.CharField(max_length=100, default="")
    description = models.TextField()
    normalized_skill_name = models.CharField(max_length=150, default="")
    importance = models.DecimalField(max_digits=3, decimal_places=2,
                                     default=0.00)
    level = models.DecimalField(max_digits=4, decimal_places=2,
                                default=0.00)
    score = models.DecimalField(max_digits=5, decimal_places=2,
                                default=0.00)
    job = models.ForeignKey(Job, on_delete=models.CASCADE,
        related_name='skills')

    def __str__(self):
        return '{}'.format(self.skill_name)

    class Meta:
        ordering = ['-score']
