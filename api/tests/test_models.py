from django.test import TestCase

from api.models import Job, Skill


class ModelJobTestCase(TestCase):
    """This class defines the test suite for the job model."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.job_title = "Software Engineer"
        self.job_location = "New York"
        self.job = Job(title=self.job_title,
                       location=self.job_location)

    def test_model_can_create_a_job(self):
        """Test the job model can create a job."""
        old_count = Job.objects.count()
        self.job.save()
        new_count = Job.objects.count()
        self.assertNotEqual(old_count, new_count)

    def test_model_returns_readable_representation(self):
        """Test a readable string is returned for the model instance."""
        self.assertEqual(str(self.job), self.job_title + ' in ' + \
                         self.job_location)


class ModelSkillsTestCase(TestCase):
    """This class defines the test suite for the skills model."""

    def setUp(self):
        """Define the test client and other test variables."""

        self.job_title = "Software Engineer"
        self.job_location = "New York"
        self.job = Job.objects.create(title=self.job_title,
                                      location=self.job_location)
        self.skill_uuid = "b3cb1294905e001d3d611bff1de3962c"
        self.skill_name = "computers and electronics"
        self.skills = Skill(skill_uuid=self.skill_uuid,
                            skill_name=self.skill_name, job=self.job)

    def test_model_can_create_a_skill(self):
        """Test the job model can create a skill."""
        old_count = Skill.objects.count()
        self.skills.save()
        new_count = Skill.objects.count()
        self.assertNotEqual(old_count, new_count)

    def test_model_returns_readable_representation(self):
        """Test a readable string is returned for the model instance."""
        self.assertEqual(str(self.skills), self.skill_name)
