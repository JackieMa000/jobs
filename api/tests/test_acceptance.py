from typing import List
from unittest import mock

from django.test import TestCase
from api.models import Job
from rest_framework.test import APIClient
from rest_framework import status
from django.urls import reverse

from api.tests.data.utils import get_data_from_json


class ACTestCase(TestCase):

    @staticmethod
    @mock.patch('api.services.api.skills')
    @mock.patch('api.services.api.job')
    def create_job(title: str, location: str, mock_job, mock_skills):
        mock_job.get.return_value = get_data_from_json(f'job_result.json')
        mock_skills.get.return_value = get_data_from_json(f'skills_result.json')['skills']
        data = {'title': title, 'location': location}
        return APIClient().post(reverse('jobs'), data, format="json")

    def assert_job_saved(self, title: str):
        job = Job.objects.get(title=title)
        self.assertEqual(title, job.title)
        self.assertEqual(title.lower(), job.normalized_title)
        self.assertTrue(job.skills.exists())

    def test_post_job(self):
        response = self.create_job('Software Architect', 'Shanghai')
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)
        self.assert_job_saved('Software Architect')

    def assert_get_jobs_response_key_value(self, response, keys):
        for item in response:
            for k in keys:
                self.assertNotEqual('', item.get(k, None))

    def test_get_jobs(self):
        keys = ['title', 'normalized_title', 'location', 'title_id']
        self.create_job('Software Architect', 'Shanghai')
        response = self.client.get(reverse('jobs'), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assert_get_jobs_response_key_value(response.json(), keys)

    def assert_response_key_value(self, response, dict1):
        for key, value in dict1.items():
            self.assertEqual(value, response.json().get(key))

    def test_get_job_detail(self):
        title = 'Software Architect'
        location = 'Shanghai'
        dict1 = {'title': title, 'normalized_title': title.lower(), 'location': location}
        self.create_job(title, location)
        response = self.client.get(
            reverse('job_detail',
                    kwargs={'title_id': Job.objects.get(title=title).title_id}), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assert_response_key_value(response, dict1)
        self.assertIsNotNone(response.json()['skills'])

    def assert_skills_ordered(self, response_data: List[dict]):
        for i in range(1, len(response_data)):
            self.assertTrue(float(response_data[i - 1]['score']) >= float(response_data[i]['score']))

    def test_get_top_skills(self):
        title = 'Software Architect'
        self.create_job(title, 'Shanghai')
        response = self.client.get(
            reverse('top_skills',
                    kwargs={'title_id': Job.objects.get(title=title).title_id}), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(10, len(response.json()))
        self.assert_skills_ordered(response.json())
