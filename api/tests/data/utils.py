import json

JSON_FILE_PATH = 'api/tests/data/'


def get_data_from_json(filename):
    with open(f'{JSON_FILE_PATH}{filename}') as f:
        data = json.load(f)
    return data
