from rest_framework import serializers
from . import models


class SkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Skill
        fields = ('skill_uuid', 'skill_name', 'skill_type',
                  'description', 'normalized_skill_name',
                  'importance', 'level', 'score')


class JobSerializer(serializers.ModelSerializer):
    skills = SkillSerializer(many=True)

    class Meta:
        model = models.Job
        fields = ('title', 'title_id', 'location', 'normalized_title',
                  'date_created', 'date_modified', 'skills')

    def create(self, validated_data):
        skills_data = validated_data.pop('skills')
        job = models.Job.objects.create(**validated_data)
        for skill_data in skills_data:
            models.Skill.objects.create(job=job, **skill_data)
        return job


class JobsSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Job
        fields = ('title', 'title_id', 'location', 'normalized_title')
