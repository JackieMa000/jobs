from rest_framework.urlpatterns import format_suffix_patterns
from django.urls import path, re_path
from . import views

urlpatterns = {
    path('jobs/', views.JobList.as_view(), name='jobs'),
    re_path(r'^jobs/(?P<title_id>[0-9A-Fa-f-]+)/$',
        views.JobDetail.as_view(), name="job_detail"),
    re_path(r'^jobs/(?P<title_id>[0-9A-Fa-f-]+)/top_skills/$',
        views.TopSkills.as_view(), name="top_skills"),
}

urlpatterns = format_suffix_patterns(urlpatterns)
