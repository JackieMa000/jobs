from django.http import JsonResponse
from rest_framework.views import APIView

from api.services import get_top_skills, get_job_by_id, generate_job_data, get_job_list, verify_request_input
from .serializers import JobsSerializer
from .serializers import JobSerializer, SkillSerializer


class JobList(APIView):
    def get(self, request, format=None):
        list_serializer = JobsSerializer(get_job_list(), many=True)
        return JsonResponse(list_serializer.data, safe=False)

    @verify_request_input('title', 'location')
    def post(self, request, format=None):
        job_name = request.data['title']
        location = request.data['location']
        serializer = JobSerializer(data=(generate_job_data(job_name, location)))
        if not serializer.is_valid():
            return JsonResponse(serializer.errors, status=400)
        serializer.save()
        return JsonResponse(serializer.data, status=201)


class JobDetail(APIView):
    def get(self, request, title_id):
        job = get_job_by_id(title_id)
        serializer = JobSerializer(job)
        return JsonResponse(serializer.data, safe=False)


class TopSkills(APIView):
    def get(sef, request, title_id):
        skills_data = get_top_skills(title_id)
        list_serializer = SkillSerializer(skills_data, many=True)
        return JsonResponse(list_serializer.data, safe=False)
