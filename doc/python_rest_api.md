# Task Description

We want to implement an API for creating jobs and populating 
the necessary skills for them, as well as (optionally) finding related jobs 
with similar skills on GitHub.

For each job created via our API we'll find a matching job title and skills 
using the [Open Skills API](https://github.com/workforce-data-initiative/skills-api/wiki/API-Overview)
and then search on GitHub to find the most relevant jobs.

## API structure

* `GET|POST /jobs`
    * `GET|PATCH|DELETE /jobs/:id:`
        * `GET /jobs/:id:/top_skills`
        * `GET /jobs/:id:/related_jobs`
        
# Acceptance Criteria

## Scenario 1: User creates a job for `Software Architect` in `New York`

1. **Given** no jobs have been saved so far
    * **when** `POST /jobs` is requested with the following data:
    ```json
    {"title": "Software Architect", "location": "New York"}
    ```
    * **then** we save the following job in the database:
        * "title": "Software Architect"
        * "location": "New York"
        * "title_id": "6b0b1cc3de799472d984e6346b929e51"
        * "normalized_title": "software architect"
        * "skills": [[...](./skills.json)]
        
The skills come from the Open Skills API, in order to get the skills for a job 
we need to:

1. [Find jobs starting with "Software Architect"](http://api.dataatwork.org/v1/jobs/autocomplete?begins_with="Software Architect")
1. Select the first result to populate:
    * "title_id": "6b0b1cc3de799472d984e6346b929e51"
    * "normalized_title": "software architect"
1. [Find related skills for this job title](http://api.dataatwork.org/v1/jobs/6b0b1cc3de799472d984e6346b929e51/related_skills)
1. Save the job and related skills into the DB

#### Score calculation

As you see, the API returns only the `importance` and `level` for each skill, 
but in our API we also have `score`.

The way score is calculated is a combination of importance and level where:

* `importance` is on a scale `0-5`
* `level` is on a scale `0-10`
* `level` has 50% more weight than `importance`

For tips, read [this article](http://mathforum.org/library/drmath/view/72033.html).

#### Extra challenge (optional):

Instead of choosing the first result, choose the one with the closest match,
for example, for [Software Engineer](http://api.dataatwork.org/v1/jobs/autocomplete?begins_with="Software Engineer") 
it will be the third result:

```json
    {
        "uuid": "4b7281a983a4574eb887d29f7fbebd88",
        "suggestion": "Software Engineer",
        "normalized_job_title": "software engineer",
        "parent_uuid": "6b6e7eda0f21fd2d8e1edf2394523743"
    }
```

## Scenario 2: User lists the jobs

1. **Given** a job for `Software Architect` in `New York` has been created
    * **when** `GET /jobs` is requested
    * **then** the following response is returned:
    ```json
    [{
        "title": "Software Architect", 
        "location": "New York", 
        "title_id": "6b0b1cc3de799472d984e6346b929e51",
        "normalized_title": "software architect"
    }]
    ```
1. **Given** a job for `Software Architect` in `New York` has been created
    * **when** `GET /jobs/6b0b1cc3de799472d984e6346b929e51` is requested
    * **then** the following response is returned:
    ```json
    {
        "title": "Software Architect", 
        "location": "New York", 
        "title_id": "6b0b1cc3de799472d984e6346b929e51",
        "normalized_title": "software architect",
        "skills": [[...](./skills.json)]
    }
    ```
1. **Given** a job for `Software Architect` in `New York` has been created
    * **when** `GET /jobs/6b0b1cc3de799472d984e6346b929e51/top_skills` is requested
    * **then** the [following response](./skills.json) is returned
        * **and** it has only the 10 top skills
        * **and** those skills are ordered by `score` (highest to lowest)